<?php

/**
 * Тестовое задание
 *
 * @author Oleg Pyatin oleg.pyatin.91@gmail.com
 */

define('ROOT_PATH', realpath($_SERVER['DOCUMENT_ROOT']).DIRECTORY_SEPARATOR);
define('LAYOUT_PATH', ROOT_PATH.'app'.DIRECTORY_SEPARATOR.'layout'.DIRECTORY_SEPARATOR);

require ROOT_PATH . 'config/bootstrap.php';
$config = require ROOT_PATH . 'config/main.php';

(new Application($config))->run($config);
