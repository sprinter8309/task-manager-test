<?php

/**
 * Класс для работы с сущностями пользователей
 *
 * @author Oleg Pyatin oleg.pyatin.91@gmail.com
 */

class User
{
    private const USER_FILE_PATH = ROOT_PATH.'data/users.json';

    public function checkLogin()
    {
        $login = Request::getParam('user_name');
        $password = Request::getParam('password');

        $validate_input = Validator::validateLoginData([
            'login'=>$login,
            'password'=>$password,
        ]);

        if (!$validate_input['correctly']) {
            return $validate_input;

        } else {
            return $this->checkUserOnInputDataExist([
                'login'=>$login,
                'password'=>$password
            ]);
        }
    }


    private function checkUserOnInputDataExist($user_data)
    {
        $file = new CommonDataManager(Application::getDataManagerParams(), ['type'=>ENTITY_USER_MARK]);

        $users_set = $file->getAllFileData();

        $identityCheckResults = $this->checkRecordsIdentity($user_data, $users_set);

        $correctly_login = $identityCheckResults['correctly_login'];
        $correctly_password = $identityCheckResults['correctly_password'];

        $message = $this->getErrorMessages($correctly_login, $correctly_password);

        if ($correctly_login && $correctly_password) {
            return [
                'correctly'=>true
            ];
        } else {
            return [
                'correctly'=>false,
                'message'=>$message
            ];
        }
    }

    private function checkRecordsIdentity($user_data, $users_set)
    {
        $correctly_login = false;
        $correctly_password = false;

        for ($i=0; $i<count($users_set); $i++) {

            if ($user_data['login']===$users_set[$i]['user_name']) {
                $correctly_login = true;

                if ($user_data['password']===$users_set[$i]['password']) {
                    $correctly_password = true;
                }
            }
        }

        return [
            'correctly_login'=>$correctly_login,
            'correctly_password'=>$correctly_password
        ];
    }

    private function getErrorMessages($correctly_login, $correctly_password)
    {
        $message = [];

        if (!$correctly_login) {
            $message[] = NON_EXIST_USER;
        } else {

            if (!$correctly_password) {
                $message[] = ERROR_PASSWORD;
            }
        }

        return $message;
    }
}
