<?php

/**
 * Класс для работы с сущностями моделей
 *
 * @author Oleg Pyatin oleg.pyatin.91@gmail.com
 */

class Task
{
    private const PAGE_SIZE = 3;

    private const TASK_FILE_PATH = ROOT_PATH.'data/tasks.json';

    private $sort_type; 

    private $sort_order;

    public function getPart($number_part)
    {
        $file = new CommonDataManager(Application::getDataManagerParams(), ['type'=>ENTITY_TASK_MARK]);

        $loaded_tasks_set = $file->getAllFileData();

        $full_tasks_set = $this->paramsNameAnalyze(Application::getDataManagerParams(), $loaded_tasks_set);

        foreach ($full_tasks_set as $key=>&$value) {
            $value+=['id'=>$key];
        }

        $this->sort_type = UserProperties::getSortType();
        $this->sort_order = UserProperties::getSortValue();

        usort($full_tasks_set, ['Task', 'swapTasksElem']);

        $part_tasks_set = array_slice($full_tasks_set, (($number_part) * static::PAGE_SIZE), static::PAGE_SIZE);

        return $part_tasks_set;
    }

    public static function getPageQuantity()
    {
        $file = new CommonDataManager(Application::getDataManagerParams(), ['type'=>ENTITY_TASK_MARK]);
        $quant_records = count($file->getAllFileData());

        $quant_of_pages = (int)floor($quant_records/static::PAGE_SIZE);
        if ($quant_records%static::PAGE_SIZE) {
            $quant_of_pages++;
        }
        return $quant_of_pages;
    }

    public function checkInputTaskData($new_task_params)
    {
        $validate_input = Validator::validateTaskData([
            'user'=>$new_task_params['user'],
            'email'=>$new_task_params['email'],
            'description'=>$new_task_params['description']
        ]);

        return $validate_input;
    }

    public function checkUpdateTaskData($update_task_params)
    {
        $validate_update = Validator::validateTaskUpdateData([
            'description'=>$update_task_params['description']
        ]);

        return $validate_update;
    }

    public function addTask($params)
    {
        $file = new CommonDataManager(Application::getDataManagerParams(), ['type'=>ENTITY_TASK_MARK]);

        $file->saveNewRecord([
            'user_name'=>Validator::XssDefend($params['user']),
            'email'=>Validator::XssDefend($params['email']),
            'description'=>Validator::XssDefend($params['description']),
            'status'=>STATUS_IN_PROGRESS,
            'redacted'=>MARK_NON_REDACTED
        ]);
    }

    public function updateTask($params)
    {
        $file = new CommonDataManager(Application::getDataManagerParams(), ['type'=>ENTITY_TASK_MARK]);

        $file->updateRecord([
            'description'=>Validator::XssDefend($params['description']),
            'status'=>(int)(!$params['complete']),
            'id'=>$params['id'],
            'redacted'=>1,
        ]);
    }

    public function getRecordInfoForUpdate($id)
    {
        $file = new CommonDataManager(Application::getDataManagerParams(), ['type'=>ENTITY_TASK_MARK]);

        $record = $file->getRecordData($id);

        return $this->paramsNameAnalyze(Application::getDataManagerParams(), $record);
    }

    private function swapTasksElem($task_one, $task_two)
    {
        $case_result = strcasecmp($task_one[$this->sort_type], $task_two[$this->sort_type]);
        return $this->sort_order * $case_result;
    }


    private function paramsNameAnalyze($config_params, $data_set)
    {
        $modified_set = [];

        switch ($config_params['current_type']) {
            case DATABASE_MANAGER_TYPE:

                foreach ($data_set as &$value) {
                    $value['user'] = $value['user_name'];
                    unset($value['user_name']);
                }

                $modified_set = $data_set;
                break;

            case JSON_MANAGER_TYPE:
                foreach ($data_set as &$value) {
                    $value['user'] = $value['user_name'];
                    unset($value['user_name']);
                }
                $modified_set = $data_set;
                break;

            default:
                break;
        };

        return $modified_set;
    }
}
