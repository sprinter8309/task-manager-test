<?php

/**
 * Предназначен для выполнения валидации данных
 *
 * @author Oleg Pyatin oleg.pyatin.91@gmail.com
 */

class Validator
{
    private const LOGIN_FIELDS_MESSAGE_MAP = [
        'login'=>USER_EMPTY_DATA,
        'password'=>PASSWORD_EMPTY_DATA,
    ];

    private const TASK_FIELDS_MESSAGE_MAP = [
        'user'=>USER_EMPTY_DATA,
        'email'=>EMAIL_EMPTY,
        'description'=>DESCRIPTION_EMPTY
    ];

    public static function validateLoginData($login_data)
    {
        return static::checkEmptyFields(static::LOGIN_FIELDS_MESSAGE_MAP, $login_data);
    }

    public static function validateTaskData($task_data)
    {
        $check_empty_fields = static::checkEmptyFields(static::TASK_FIELDS_MESSAGE_MAP, $task_data);

        if ($check_empty_fields['correctly']) {

            if (static::checkEmail($task_data['email'])) {
                return [
                    'correctly'=>true
                ];
            } else {
                return [
                    'correctly'=>false,
                    'message'=>[EMAIL_FAILED]
                ];
            }

        } else {
            return $check_empty_fields;
        }
    }

    public static function XssDefend($text)
    {
        $text_striped = strip_tags($text);
        $text_special = htmlspecialchars($text_striped);
        return stripslashes($text_special);
    }

    public static function validateTaskUpdateData($update_task_data)
    {
        if (empty($update_task_data['description'])) {
            return [
                'correctly'=>false,
                'message'=>[DESCRIPTION_EMPTY]
            ];
        } else {
            return [
                'correctly'=>true
            ];
        }
    }

    private static function checkEmptyFields($fields_map, $data_set)
    {
        $message = [];

        foreach ($fields_map as $key => $value) {
            if (empty($data_set[$key])) {
                $message[] = $value;
            }
        }

        return [
            'correctly'=>!(count($message)>0),
            'message'=>$message
        ];
    }

    private static function checkEmail($email)
    {
        return (bool)preg_match('/.+@.+\..+/i', $email);
    }
}
