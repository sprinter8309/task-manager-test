<!DOCTYPE HTML>
<html lang="ru">
    <head>
        <meta charset="utf8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Language" content="ru">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Тестовое задание</title>

        <link rel="stylesheet" type="text/css" href="/app/layout/css/style.css">
        <script src="/app/layout/js/script.js" defer></script>

    </head>
    <body>
        <div class="page">
            <div class="center">
                <header class="content-part">
                    <h1><?= $title ?></h1>
                </header>
                <main class="content-part">
                    <?= $content ?>
                </main>
            </div>
        </div>
    </body>
</html>