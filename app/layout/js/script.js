function getPartTaskList(params)
{
    var xhttp = new XMLHttpRequest();
    
    xhttp.open('GET', '/part?'+params, true);
    xhttp.send();

    xhttp.onreadystatechange = function() {

        var taskListContainer = document.querySelector(".task-content-container");

        if ( this.readyState != 4 ) { 
            return;
        } else {	
            taskListContainer.innerHTML = this.responseText;
            bindPaginationActions()
        }
    }
}

function bindPaginationActions()
{
    var pagination_labels = document.querySelectorAll(".pagination > a");
     
    for (var i = 0; i < (pagination_labels.length); i++) {
        
        pagination_labels[i].addEventListener('click', function(event) {
            
            event.preventDefault();
            var numHref = this.getAttribute('href');
            getPartTaskList('id='+numHref)
        })
    }   
}

function initialize() 
{
    var labelSortName = document.querySelector(".label-sort-name");
    var labelSortEmail = document.querySelector(".label-sort-email");
    var labelSortStatus = document.querySelector(".label-sort-status");
    var labelSortAscOrder = document.querySelector(".label-sort-asc");
    var labelSortDescOrder = document.querySelector(".label-sort-desc");
    
    var main_container = document.querySelector(".task-content-container");

    labelSortName.onclick = function() {
        getPartTaskList('sort=user')
    }
    labelSortEmail.onclick = function() {
        getPartTaskList('sort=email')
    }
    labelSortStatus.onclick = function() {
        getPartTaskList('sort=status')
    }
    labelSortAscOrder.onclick = function() {
//        console.log('T400')
        getPartTaskList('order=asc')
    }
    labelSortDescOrder.onclick = function() {
        getPartTaskList('order=desc')
    }

    bindPaginationActions()
    
    if (main_container.classList.contains("success-operation")) {
        alert('Операция успешно выполнена');
    }
}

document.addEventListener("DOMContentLoaded", initialize);
