<?php
Application::setParam('title', 'Задачи');
?>
<?php if (isset($start_render)): ?>
<div class="control-panel">
    <a href="/create" class="new-task-button control-panel-button">Создать задачу</a>
    <div class="sort-panel">
        <span class="label-sort-info">Сортировать по:</span>
        <span class="label-sort-name">Имени</span>
        <span class="label-sort-email">Email</span>
        <span class="label-sort-status">Статусу</span>
        <span class="label-sort-asc">&#8595;</span>
        <span class="label-sort-desc">&#8593;</span>
    </div>
    <div class="auth-panel">
        <?php if (UserProperties::isGuest()): ?>
            <a class="control-panel-button" href="/login">Войти</a>
        <?php else: ?>
            <a class="control-panel-button logout-button" href="/logout">Выйти</a>
        <?php endif; ?>
    </div>
</div>
<?php endif; ?>
<div class="task-content-container <?php if ($success_operation ?? false) { echo "success-operation"; } ?>">
    <div>
        <?php foreach($tasks as $task): ?>

            <div class="task-item">
                <div class="task-item-line">
                    Пользователь - <?= $task['user'] ?>
                </div>
                <div class="task-item-line">
                    Email - <?= $task['email'] ?>
                </div>
                <div class="task-item-description">
                    <div class="task-item-description-field"><?= $task['description'] ?></div>
                </div>
                <div class="status-label">
                    <?php if ($task['redacted']): ?>
                        <span class="status-label-text status-label-redacted">Отредактировано администратором </span>
                    <?php endif; ?>

                    <?php if (!$task['status']): ?>
                        <span class="status-label-text status-label-success">Выполнено</span>
                    <?php else: ?>
                        <span class="status-label-text status-label-in-progress">В процессе</span>
                    <?php endif; ?>
                </div>
                <?php if (!UserProperties::isGuest()): ?>
                    <a class="link-task-redact" href="/change?id=<?= $task['id'] ?>">Отредактировать задачу</a>
                <?php endif; ?>
            </div>

        <?php endforeach; ?>
    </div>
    <div class="pagination">
        <?php
            for ($i=0; $i<$pagination; $i++) {
                echo '<a href="'.($i+1).'">'.($i+1).'</a>';
            }
        ?>
    </div>
</div>