<div class="login-panel">
    <?php if (UserProperties::isGuest()): ?>
        <div class="message-panel">
            <?php if (!empty($message)): ?>
                <?php foreach ($message as $label): ?>
                    <div><?= $label ?></div>
                <?php endforeach; ?>
            <?php else: ?>
                <div class="simple-auth-label">Введите входные данные:</div>
            <?php endif; ?>
        </div>
        <form name="login-form" class="login-form" method="POST" action="/login">
            <input type="text" name="user_name" value="" placeholder="Имя пользователя">
            <input type="password" name="password" value="" placeholder="Пароль">
            <input type="submit" value="Отправить">
        </form>
    <?php else: ?>
        <h2 class="info-label">Вы уже авторизованы</h2>
        <div class="auth-panel">
            <?php if (!UserProperties::isGuest()): ?>
                <a class="control-panel-button logout-button" href="/logout">Выйти</a>
            <?php endif; ?>
        </div>
    <?php endif; ?>
    <a class="return-button" href="/">Вернуться к списку</a>
</div>