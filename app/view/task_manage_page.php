<div class="task-manage-panel">
    <div class="message-panel">
        <?php if (!empty($message)): ?>
            <?php foreach ($message as $label): ?>
                <div><?= $label ?></div>
            <?php endforeach; ?>
        <?php else: ?>
            <div class="simple-auth-label">Введите данные для задачи:</div>
        <?php endif; ?>
    </div>
    <form name="manage-task-form"
          class="manage-task-form"
          method="POST"
          action="<?= $query ?>">
        <input type="text" name="user" value="<?= ($user)??'' ?>" placeholder="Имя пользователя" <?php if ($type!=='create') { echo "disabled"; } ?>>
        <input type="text" name="email" value="<?= ($email)??'' ?>" placeholder="Email" <?php if ($type!=='create') { echo "disabled"; } ?>>
        <textarea name="description" class="manage-task-form-field"><?= ($description)??'' ?></textarea>
        <?php if ($type!=='create'): ?>
            <div class="success-block">
                <input type="hidden" name="id" value="<?= $id ?>">
                <input type="checkbox" name="complete" value="1" <?php if ($success) { echo "checked"; } ?>>
                <span>Задача выполнена</span>
            </div>
        <?php endif; ?>
        <input type="submit" value="Сохранить">
    </form>
    <a class="return-button" href="/">Вернуться к списку</a>
    <?php if (!UserProperties::isGuest()): ?>
        <div class="auth-panel">
            <?php if (!UserProperties::isGuest()): ?>
                <a class="control-panel-button logout-button" href="/logout">Выйти</a>
            <?php endif; ?>
        </div>
    <?php endif; ?>
</div>
