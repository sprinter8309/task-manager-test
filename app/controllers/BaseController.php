<?php

/**
 * Контроллер для выполнения действия связанных с задачами
 *
 * @author Oleg Pyatin oleg.pyatin.91@gmail.com
 */

class BaseController
{
    private const START_SET_NAME = 'start';

    private const PAGE_SET_NAME = 'page';

    private const CREATION_MANAGE_ACTION = 'create';

    private const UPDATE_MANAGE_ACTION = 'update';

    private const TASKS_LIST_PAGE_NAME = 'Задачи';

    private const CREATE_LIST_PAGE_NAME = 'Создание задачи';

    private const UPDATE_LIST_PAGE_NAME = 'Обновление задачи';

    public function index()
    {
        UserProperties::setCurrentPageNumber(1);

        $tasks_start_set = $this->getTaskSet(static::START_SET_NAME);

        $pagination = Task::getPageQuantity();

        $success_operation = UserProperties::readMarkSuccessOperation();

        Application::setParam('title', static::TASKS_LIST_PAGE_NAME);

        Viewer::renderFull('app/view/task_window.php', [
            'tasks'=>$tasks_start_set,
            'pagination'=>$pagination,
            'start_render'=>true,
            'success_operation'=>$success_operation
        ]);
    }

    public function part()
    {
        $tasks_page_set = $this->getTaskSet(static::PAGE_SET_NAME);

        $pagination = Task::getPageQuantity();

        Viewer::renderPartView('app/view/task_window.php', [
            'tasks'=>$tasks_page_set,
            'pagination'=>$pagination
        ]);
    }

    public function create()
    {
        Application::setParam('title', static::CREATE_LIST_PAGE_NAME);

        if (Request::getParam('method')==='post') {

            $task = new Task();

            $user = Request::getParam('user');
            $email = Request::getParam('email');
            $description = Request::getParam('description');

            $check_result = $task->checkInputTaskData([
                'user'=>$user,
                'email'=>$email,
                'description'=>$description,
            ]);

            if ($check_result['correctly']) {

                $task->addTask([
                    'user'=>$user,
                    'email'=>$email,
                    'description'=>$description,
                ]);

                UserProperties::setWindowSuccessOperation();
                header('Location: /index');

            } else {

                Viewer::renderFull('app/view/task_manage_page.php', [
                    'query'=>'/create',
                    'type'=>'create',
                    'message'=>$check_result['message'],
                    'user'=>$user,
                    'email'=>$email,
                    'description'=>$description
                ]);
            }

        } else {

            Viewer::renderFull('app/view/task_manage_page.php', [
                'query'=>'/create',
                'type'=>'create'
            ]);
        }
    }

    public function change()
    {
        Application::setParam('title', static::UPDATE_LIST_PAGE_NAME);

        $task = new Task();

        if (Request::getParam('method')==='post') {

            $id = Request::getParam('id');
            $description = Request::getParam('description');
            $complete = (int)(Request::getParam('complete') ?? 0);

            $check_result = $task->checkUpdateTaskData([
                'description'=>$description,
                'complete'=>$complete
            ]);

            if ($check_result['correctly']) {

                var_dump(UserProperties::isGuest());


                if (UserProperties::isGuest()) {
                    header('Location: /login');
                    return;
                }

                $task->updateTask([
                    'description'=>$description,
                    'complete'=>$complete,
                    'id'=>$id
                ]);

                UserProperties::setWindowSuccessOperation();
                header('Location: /index');

            } else {

                $record_data = $task->getRecordInfoForUpdate($id);

                Viewer::renderFull('app/view/task_manage_page.php', [
                    'query'=>'/change',
                    'type'=>'change',
                    'message'=>$check_result['message'],
                    'id'=>$id,
                    'success'=>$record_data['status'],
                    'user'=>$record_data['user'],
                    'status'=>$record_data['status'],
                    'description'=>$record_data['description'],
                ]);
            }

        } else {

            $id = Request::getParam('id');

            $record_data = $task->getRecordInfoForUpdate($id)[0];

            Viewer::renderFull('app/view/task_manage_page.php', [
                'query'=>'/change',
                'type'=>'change',
                'message'=>'',
                'id'=>$id,
                'success'=>!$record_data['status'],
                'user'=>$record_data['user'],
                'status'=>$record_data['status'],
                'description'=>$record_data['description'],
            ]);
        }
    }

    public function error()
    {
        Viewer::renderFull('app/view/error_page.php', []);
    }

    private function getTaskSet($type)
    {
        $tasks = new Task();

        switch ($type) {
            case static::START_SET_NAME:
                return $tasks->getPart(0);
                break;
            case static::PAGE_SET_NAME:

                $manage_list_action = Request::getFirstParam();
                $this->changeTaskList($manage_list_action);

                return $tasks->getPart((UserProperties::getCurrentPageNumber())-1);
                break;
            default:
                return [];
        }

    }

    private function manageTasks($type)
    {
        $tasks = new Task();

        switch ($type) {
            case static::CREATION_MANAGE_ACTION:

                break;
            case static::UPDATE_MANAGE_ACTION:
                return $tasks->getPart(Request::getParam('id'));
                break;
            default:
                return [];
        }
    }

    private function changeTaskList($manage_list_action)
    {
        switch ($manage_list_action['name']) {
            case GET_PARAM_PAGE_SCROLL_NAME:
                UserProperties::setCurrentPageNumber($manage_list_action['value']);
                break;
            case GET_PARAM_SORT_TYPE_NAME:
                UserProperties::setSortType($manage_list_action['value']);
                break;
            case GET_PARAM_SORT_ORDER_NAME:
                UserProperties::setSortOrder($manage_list_action['value']);
                break;
            default:
                break;
        }
    }

    private function successMessage()
    {
        echo '<script>alert('.MESSAGE_NEW_TASK_SUCCESS_ADD.')</script>';
    }
}
