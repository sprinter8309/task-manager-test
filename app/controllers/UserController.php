<?php

/**
 * Контроллер для действий связанных с авторизацией
 *
 * @author Oleg Pyatin oleg.pyatin.91@gmail.com
 */
class UserController
{
    private const AUTH_PAGE_NAME = 'Авторизация';

    private const START_MESSAGE = '';

    private const ERROR_MESSAGE = 'Ошибка во входных данных';

    public function login()
    {
        Application::setParam('title', static::AUTH_PAGE_NAME);

        if (Request::getParam('method')==='post') {

            $user = new User();

            // Общая проверка введенных данных
            $check_result = $user->checkLogin();

            if ($check_result['correctly']) {

                UserProperties::setUserName(Request::getParam('user_name'));
                header('Location: /index');

            } else {

                Viewer::renderFull('app/view/login_page.php', [
                    'message'=>$check_result['message']
                ]);
            }

        } else {

            Viewer::renderFull('app/view/login_page.php', []);
        }
    }

    public function logout()
    {
        UserProperties::userLogout();
        header('Location: /index');
    }
}
