<?php

/**
 * Предназначен для выполнения работы с json-файлом
 *
 * @author Oleg Pyatin oleg.pyatin.91@gmail.com
 */

class JsonManager
{
    private const DEFAULT_PAGE_SIZE = 3;

    private const NO_LOCK_EVENT = 'Не удалось получить блокировку файла для записи';

    private const CREATE_OPERATION_TYPE = 1;

    private const UPDATE_OPERATION_TYPE = 2;

    private $file_path;

    public function __construct($params)
    {
        $this->file_path = $params['path'];
    }

    public function getFilePart($page_number)
    {
        $json_file = file_get_contents($this->file_path);
        $json_data = json_decode($json_file, true);

        return array_slice($json_data, ($page_number * $this->page_size), $this->page_size);
    }

    public function getAllFileData()
    {
        $json_file = file_get_contents($this->file_path);
        return json_decode($json_file, true);
    }

    public function getRecordData($id)
    {
        $json_data = file_get_contents($this->file_path);
        $json_array = json_decode($json_data, true);
        return [
            0=>$json_array[$id]
        ];
    }

    public function saveNewRecord($new_record)
    {
        $this->changeRecord($new_record, static::CREATE_OPERATION_TYPE);
    }

    public function updateRecord($updated_record)
    {
        $this->changeRecord($updated_record, static::UPDATE_OPERATION_TYPE);
    }

    private function changeRecord($changed_record, $type)
    {
        $json_handle = fopen($this->file_path, 'r+');

        if (flock($json_handle, LOCK_EX)) {

            $json_data = fread($json_handle, filesize($this->file_path));
            ftruncate($json_handle, 0);

            $json_array = json_decode($json_data, true);

            switch ($type) {
                case static::CREATE_OPERATION_TYPE:
                    $json_array = $this->addOperation($json_array, $changed_record);
                    break;
                case static::UPDATE_OPERATION_TYPE:
                    $json_array = $this->updateOperation($json_array, $changed_record);
                    break;
                default:
                    break;
            }

            $prepared_json_data = json_encode($json_array);

            rewind($json_handle);
            fwrite($json_handle, trim($prepared_json_data));

            fflush($json_handle);
            flock($json_handle, LOCK_UN);

        } else {
            throw new Exception(static::NO_LOCK_EVENT);
        }

        fclose($json_handle);
    }

    private function addOperation($array_set, $new_record)
    {
        $new_set = $array_set;
        $new_set[] = $new_record;
        return $new_set;
    }

    private function updateOperation($array_set, $updated_record)
    {
        $new_set = $array_set;

        if (isset($updated_record['description'])) {
            $new_set[$updated_record['id']]['description'] = $updated_record['description'];
        }
        if (isset($updated_record['status'])) {
            $new_set[$updated_record['id']]['status'] = $updated_record['status'];//($updated_record['complete']>0) ? 0 : 1;
        }
        $new_set[$updated_record['id']]['redacted'] = 1;

        return $new_set;
    }
}
