<?php

/**
 * Предназначен для создания каркаса компонентов работы с источниками данных
 *
 * @author Oleg Pyatin oleg.pyatin.91@gmail.com
 */

interface DataManager
{
    public function getAllFileData();
    public function getRecordData($id);
    public function saveNewRecord($record);
    public function updateRecord($updated_record);
}
