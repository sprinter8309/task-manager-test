<?php

/**
 * Класс используемый как общая точка доступа для текущей выбранного источника данных
 *
 * @author Oleg Pyatin oleg.pyatin.91@gmail.com
 */

class CommonDataManager implements DataManager
{
    private $data_manager;

    public function __construct($data_manager_config, $additional_params)
    {
        switch ($data_manager_config['current_type']) {
            case JSON_MANAGER_TYPE:
                $this->data_manager = new JsonManager($data_manager_config['json'][$additional_params['type']]);
                break;
            case DATABASE_MANAGER_TYPE:
                $this->data_manager = new DatabaseManager($data_manager_config['database'], $additional_params);
                break;
            default:
                throw new \Exception(ERROR_UNKNOWN_TYPE_DATA_MANAGER);
                break;
        };
    }

    public function getAllFileData()
    {
        return $this->data_manager->getAllFileData();
    }

    public function getRecordData($id)
    {
        return $this->data_manager->getRecordData($id);
    }

    public function saveNewRecord($new_record)
    {
        $this->data_manager->saveNewRecord($new_record);
    }

    public function updateRecord($updated_record)
    {
        $this->data_manager->updateRecord($updated_record);
    }
}
