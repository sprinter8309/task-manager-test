<?php

/**
 * Предназначен для выполнения работы с базой данных
 *
 * @author Oleg Pyatin oleg.pyatin.91@gmail.com
 */

class DatabaseManager
{
    private $db;

    private $table_name;

    private const DB_CONNECTION_ERROR = 'Ошибка при соединении с БД';

    private const DB_SELECT_ERROR = 'Ошибка при запросе получения данных';

    private const DB_INSERT_ERROR = 'Ошибка при запросе добавления данных';

    private const DB_UPDATE_ERROR = 'Ошибка при запросе обновления данных';

    public function __construct($db_params, $additional_params)
    {
        try {

            // Строка соединения (дополинтельно учитываем СУБД)
            $connection = $db_params['pdo_prefix'] . ":host=" . $db_params['host']
                    . ";dbname=" . $db_params['name'] . ";options='-c client_encoding=utf8'";

            // Задание PDO опций
            $options = [
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
                \PDO::ATTR_EMULATE_PREPARES => false
            ];

            $pdo = new \PDO($connection, $db_params['user'], $db_params['password'], $options);

            $this->db = $pdo;

            $this->table_name = $db_params['table_map'][$additional_params['type']];

        } catch (\Throwable|\PDOException $exc) {

            throw new Exception(static::DB_CONNECTION_ERROR);
        }
    }

    public function getAllFileData()
    {
        $query = $this->db->prepare("SELECT * FROM $this->table_name");

        try {
            $result = $query->execute();
        } catch (\Throwable|\PDOException $exc) {
            throw new Exception(static::DB_SELECT_ERROR);
        }

        $tasks_set = [];

        foreach ($query as $row) {
            $tasks_set[] = $row;
        }

        return $tasks_set;
    }

    public function getRecordData($id)
    {
        $prepare_id = $id;

        try {
            $query = $this->db->prepare("SELECT * FROM $this->table_name WHERE id=:id");
            $query->bindParam(':id', $prepare_id);
            $result = $query->execute();
        } catch (\Throwable|\PDOException $exc) {
            throw new Exception(static::DB_SELECT_ERROR);
        }

        $tasks_set = [];

        foreach ($query as $row) {
            $tasks_set[] = $row;
        }

        return $tasks_set;
    }

    public function saveNewRecord($new_record)
    {
        try {

            $param_names = array_keys($new_record);
            $new_alias_names = implode(',', $param_names);
            $new_pdo_params = ':'.implode(',:', $param_names);

            $query_string = "INSERT INTO $this->table_name (";
            $query_string .= $new_alias_names.') VALUES (';
            $query_string .= $new_pdo_params.')';

            $query = $this->db->prepare($query_string);

            foreach ($new_record as $key=>$value) {
                $query->bindParam(":".$key, $new_record[$key]);
            }

            $result = $query->execute();
        } catch (\Throwable|\PDOException $exc) {

            throw new Exception(static::DB_SELECT_ERROR);
        }
    }

    public function updateRecord($updated_record)
    {
        $modified_record = $updated_record;
        $id = $modified_record['id'];
        unset($modified_record['id']);

        $query_string = "UPDATE $this->table_name SET ";
        foreach ($modified_record as $key=>$value) {
            $query_string.= $key.'=:'.$key.',';
        }
        $query_string = preg_replace('/,$/', '', $query_string);
        $query_string.= ' WHERE id=:id';

        try {
            $query = $this->db->prepare($query_string);
            foreach ($modified_record as $key=>$value) {
                $query->bindParam(":".$key, $modified_record[$key]);
            }
            $query->bindParam(':id', $id);
            $result = $query->execute();
        } catch (\Throwable|\PDOException $exc) {

            throw new Exception(static::DB_SELECT_ERROR);
        }
    }
}
