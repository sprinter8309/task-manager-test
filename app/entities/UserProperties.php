<?php

/**
 * Предназначен для хранения пользовательских настроек (сессионных)
 *
 * @author Oleg Pyatin oleg.pyatin.91@gmail.com
 */

class UserProperties
{
    private static $user_name;

    private static $sort_type;

    private static $sort_order;

    private static $page_number;

    private static $windows_success_operation;

    private const SORT_ORDER_MAP = [
        SORT_ASC_QUERY_NAME => SORT_ASC_VALUE,
        SORT_DESC_QUERY_NAME => SORT_DESC_VALUE
    ];

    public static function initialize()
    {
        static::$user_name = ($_SESSION['user_name']) ?? null;
        static::$sort_type = ($_SESSION['sort_type']) ?? null;
        static::$sort_order = ($_SESSION['sort_order']) ?? null;
        static::$page_number = ($_SESSION['page_number']) ?? 1;
        static::$windows_success_operation = ($_SESSION['windows_success_operation']) ?? false;
    }

    public static function isGuest()
    {
        return (empty(static::$user_name));
    }

    public static function getUserName()
    {
        return static::$user_name;
    }

    public static function setUserName($new_name)
    {
        $_SESSION['user_name'] = $new_name;
        static::$user_name = $new_name;
    }

    public static function userLogout()
    {
        unset($_SESSION['user_name']);
        static::$user_name = null;
    }

    public static function getSortType()
    {
        return (static::$sort_type ?? SORT_NAME_TYPE);
    }

    public static function setSortType($new_sort_type)
    {
        $_SESSION['sort_type'] = $new_sort_type;
        static::$sort_type = $new_sort_type;
    }

    public static function getSortValue()
    {
        return (static::SORT_ORDER_MAP[static::$sort_order] ?? static::SORT_ORDER_MAP[SORT_ASC_QUERY_NAME]);
    }

    public static function getSortOrder()
    {
        return (static::$sort_order ?? SORT_ASC_VALUE);
    }

    public static function setSortOrder($new_sort_order)
    {
        $_SESSION['sort_order'] = $new_sort_order;
        static::$sort_order = $new_sort_order;
    }

    public static function getCurrentPageNumber()
    {
        return static::$page_number;
    }

    public static function setCurrentPageNumber($new_page_number)
    {
        $_SESSION['page_number'] = $new_page_number;
        static::$page_number = $new_page_number;
    }

    public static function setWindowSuccessOperation()
    {
        $_SESSION['windows_success_operation'] = true;
        static::$windows_success_operation = true;
    }

    public static function readMarkSuccessOperation()
    {
        if (static::$windows_success_operation) {
            $_SESSION['windows_success_operation'] = false;
            static::$windows_success_operation = false;
            return true;
        } else {
            return false;
        }
    }
}
