-- Создание таблиц

create database if not exists `testing_task`;

use `testing_task`;

create table if not exists `tasks` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`user_name` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NOT NULL,
	`email` VARCHAR(128) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NOT NULL,
	`description` VARCHAR(128) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NOT NULL,
	`status` TINYINT NOT NULL,
	`redacted` BOOLEAN NOT NULL,	
	PRIMARY KEY (`id`)
)
engine = InnoDB
DEFAULT CHARACTER SET 'utf8'
COLLATE 'utf8_general_ci';

create table if not exists `users` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`user_name` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NOT NULL,
	`password` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NOT NULL,
	PRIMARY KEY (`id`)
)
engine = InnoDB
DEFAULT CHARACTER SET 'utf8'
COLLATE 'utf8_general_ci';





-- Заполнение таблиц значениями

INSERT INTO tasks VALUES (1, "Георгий", "work_task@yandex.ru", "Съездить за инструментами", 1, FALSE);
INSERT INTO tasks VALUES (2, "Елена", "steel80901k@mail.ru", "Полить цветы и приготовить торт", 1, FALSE);
INSERT INTO tasks VALUES (3, "Владимир", "bam-bam@gmail.com", "Сделать шкаф и стол для дачи", 0, FALSE);
INSERT INTO tasks VALUES (4, "Антон", "living_paradise_14@yandex.ru", "Поклеить обои, лакировать пол", 1, FALSE);
INSERT INTO tasks VALUES (5, "Олеся", "tip100.100@rambler.ru", "Собрать ягоды и помидоры", 1, FALSE);
INSERT INTO tasks VALUES (6, "Инна", "nownow931@mail.ru", "Написать стихи к юбилею предприятия", 0, FALSE);
INSERT INTO tasks VALUES (7, "Игорь", "cap_cap@gmail.com", "Взять справку о наличии прав вождении", 1, FALSE);
INSERT INTO tasks VALUES (8, "Василий", "win81094@yandex.ru", "Сходить на конференцию по развитию современного кинематографа", 1, FALSE);

INSERT INTO users VALUES (1, "admin", "123");
INSERT INTO users VALUES (2, "manager", "456");
