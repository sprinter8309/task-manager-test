<?php

define(LAYOUT_PATH, ROOT_PATH.'app/layout');

const CORRECT_INPUT_DATA = 'Входные данные имеют корректный формат';

const USER_EMPTY_DATA = 'Имя пользователя пустое';

const PASSWORD_EMPTY_DATA = 'Введенный пароль пуст';

const NON_EXIST_USER = 'Пользователя с введенным именем не существует';

const ERROR_PASSWORD = 'Ошибочный пароль';

const EMAIL_EMPTY = 'Поле email пустое';

const DESCRIPTION_EMPTY = 'Поле описания пустое';

const EMAIL_FAILED = 'Введенный email некорректный';

const SORT_ASC_QUERY_NAME = 'asc';

const SORT_DESC_QUERY_NAME = 'desc';

const SORT_ASC_VALUE = 1;

const SORT_DESC_VALUE = -1;

const SORT_NAME_TYPE = 'user';

const SORT_EMAIL_TYPE = 'email';

const SORT_STATUS_TYPE = 'status';

const GET_PARAM_PAGE_SCROLL_NAME = 'id';

const GET_PARAM_SORT_TYPE_NAME = 'sort';

const GET_PARAM_SORT_ORDER_NAME = 'order';

const CREATION_MANAGE_ACTION = 'create';

const UPDATE_MANAGE_ACTION = 'change';

const DATABASE_MANAGER_TYPE = 'database';

const JSON_MANAGER_TYPE = 'json';

const ERROR_UNKNOWN_TYPE_DATA_MANAGER = 'Error type data manager';

const ERROR_NO_ADDRESS_REQUEST = 'No necessary address';

const ERROR_TYPE_HTTP_METHOD = 'Error type of HTTP method';

const ERROR_DISPLAY_DATA = 'Error on display info step';

const STATUS_IN_PROGRESS = 1;

const STATUS_SUCCESS = 0;

const MARK_NON_REDACTED = 0;

const MESSAGE_NEW_TASK_SUCCESS_ADD = 'Новая задача была успешно добавлена';

const ENTITY_TASK_MARK = 100;

const ENTITY_USER_MARK = 200;



require ROOT_PATH.'core/InitLoader.php';
spl_autoload_register(array("InitLoader", "autoload"));

session_start();
UserProperties::initialize();
