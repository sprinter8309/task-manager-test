<?php

$routes = require_once __DIR__ . '/routes.php';

$config = [
    'data_manager_params'=>[
        'current_type'=>JSON_MANAGER_TYPE,
//        'current_type'=>DATABASE_MANAGER_TYPE,  // ЗДесь можно просто переключать тип
        'json'=>[
            ENTITY_TASK_MARK=>[
                'path'=>ROOT_PATH.'data/tasks.json'
            ],
            ENTITY_USER_MARK=>[
                'path'=>ROOT_PATH.'data/users.json'
            ],
        ],
        'database'=>[
            'table_map'=>[
                ENTITY_TASK_MARK=>'tasks',
                ENTITY_USER_MARK=>'users'
            ],
            'pdo_prefix' => 'mysql',
            'name' => 'testing_task',
            'user' => 'root',
            'password' => '5power9',
            'host' => 'localhost'
        ]
    ],
    'routes' => $routes,
    'params' => [
        'title'=>'Задачи'
    ],
];

return $config;
