<?php

return [

    'core'=>ROOT_PATH.'core',
    'classes'=>ROOT_PATH.'core'.DIRECTORY_SEPARATOR.'classes',
    'exceptions'=>ROOT_PATH.'core'.DIRECTORY_SEPARATOR.'exceptions',
    'controllers'=>ROOT_PATH.'app'.DIRECTORY_SEPARATOR.'controllers',
    'models'=>ROOT_PATH.'app'.DIRECTORY_SEPARATOR.'models',
    'components'=>ROOT_PATH.'app'.DIRECTORY_SEPARATOR.'components',
    'helpers'=>ROOT_PATH.'app'.DIRECTORY_SEPARATOR.'helpers',
    'entities'=>ROOT_PATH.'app'.DIRECTORY_SEPARATOR.'entities',
    'components_interfaces'=>ROOT_PATH.'app'.DIRECTORY_SEPARATOR.'components/interfaces'
];
