<?php

/**
 * Класс для вывода информации
 *
 * @author Oleg Pyatin oleg.pyatin.91@gmail.com
 */
class Viewer
{
    public static function renderFull($view_path, $view_params=[]):void
    {
        $params = Application::getAllParams();
        $params += [
            'content'=>static::renderView($view_path, $view_params)
        ];

        $response = static::renderAction(LAYOUT_PATH.'base.php', $params);
        echo $response;
    }

    public static function renderPartView($view_path, $view_params=[]):void
    {
        echo static::renderAction(ROOT_PATH.$view_path, $view_params);
    }

    private static function renderView($view_path, $view_params)
    {
        return static::renderAction(ROOT_PATH.$view_path, $view_params);
    }

    private static function renderAction($file_path, $params)
    {
        extract($params, EXTR_SKIP);
        ob_start();

        try {

            include $file_path;

        } catch (\Throwable $exc) {

            ob_end_clean();
            throw new \Exception(ERROR_DISPLAY_DATA);
        }
        return ob_get_clean();
    }
}
