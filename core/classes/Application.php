<?php

/**
 * Основной класс приложения, выполняет координирующую работу
 *
 * @author Oleg Pyatin oleg.pyatin.91@gmail.com
 */
class Application
{
    /**
     * Свойство для конфигурации (для удобства ее получения из методов)
     */
    protected static $config = [];

    /**
     * Основная функция приложения
     *
     * @param array $config Параметры конфигурации приложения
     */
    public static function run(array $config):void
    {
        try {

            static::$config = $config;

            $action = Request::resolveRequest();

            $response = Processor::executeAction($action);

        } catch (\Throwable $exc) {

            header('Location: /error');
        }
    }

    public static function getRoutes():array
    {
        return static::$config['routes'];
    }

    public static function getAllParams():array
    {
        return static::$config['params'];
    }

    public static function getDataManagerParams():array
    {
        return static::$config['data_manager_params'];
    }

    public static function setParam($name, $value):void
    {
        static::$config['params'][$name]=$value;
    }

    public static function getParam($name):void
    {
        static::$config['params'][$name];
    }
}
