<?php

/**
 * Предназначен для организации основной логики
 *
 * @author Oleg Pyatin oleg.pyatin.91@gmail.com
 */
class Processor
{
    public static function executeAction(array $action)
    {
        $controller = $action['controller'];
        $action_name = $action['action_name'];

        $execute_controller = new $controller();
        $response = ($execute_controller->{$action_name}());

        return $response;
    }
}
