<?php

/**
 * Получение входных данных и определения действия
 *
 * @author Oleg Pyatin oleg.pyatin.91@gmail.com
 */
class Request
{
    private static $params;

    // Анализ запроса и роутинг минимизированы - случай приложения очень малого размера
    public static function resolveRequest()
    {
        $routes = Application::getRoutes();

        $request_uri = $_SERVER['REQUEST_URI'];

        $uri_part = explode('?', $request_uri)[0];

        if (isset($routes[$uri_part])) {

            $method = $_SERVER['REQUEST_METHOD'];

            $extracted_params = [];

            switch ($method) {
                case 'GET':
                        static::$params = static::extractParams($_GET);
                        static::$params += ['method'=>'get'];
                    break;
                case 'POST':
                        static::$params = static::extractParams($_POST);
                        static::$params += ['method'=>'post'];
                    break;
                default:
                    throw new Exception(ERROR_TYPE_HTTP_METHOD);
            };

            return [
                'controller'=>$routes[$uri_part]['controller'],
                'action_name'=>$routes[$uri_part]['action_name'],
            ];

        } else {

            throw new \Exception(ERROR_NO_ADDRESS_REQUEST);
        }
    }

    private static function extractParams(?array $request_params)
    {
        $params = [];

        foreach ($request_params as $name=>$value) {
            $params+=[
                $name=>$value
            ];
        }

        return $params;
    }

    public static function getParam($param_name)
    {
        return static::$params[$param_name] ?? null;
    }

    public static function getFirstParam()
    {
        return [
            'name'=>key(static::$params),
            'value'=>current(static::$params)
        ];
    }
}
