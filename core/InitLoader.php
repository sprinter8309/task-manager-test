<?php

class InitLoader
{
    public static function autoload($class)
    {
        $class_map = require ROOT_PATH.'core/autoload.php';

        foreach ($class_map as $class_root) {

            $supposed_path = $class_root.DIRECTORY_SEPARATOR.$class.'.php';

            if (is_file($supposed_path)) {
                require_once $supposed_path;
                return true;
            }
        }

        return false;
    }
}
